var f = require('Faker');
f.locale = "pl";
var name = f.name.findName()

var users = {
	path: '/users',
	template:{
		id: f.random.number(),
		name: name,
		city: f.address.city()
	},
	status: function(req,res){
		var html;
		html += '<h1>'+ name +'</h1>';
 		res.send(200, html)
	}

};

module.exports = [users];
