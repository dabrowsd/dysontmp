var f = require('faker');
f.locale = "pl";

var city = {
	path: '/city',
	method: 'GET',
	template: {
		id: f.random.uuid(),
		city: f.address.city(),
		zipCode: f.address.zipCode()
	},
	status: function(req, res){
		res.append('Content-Type', 'application/json');
		res.status(200).send('<p>City endpoint works fine</p/'+
			'<p>'+city+'</p>')
	}

};

var cities = {
	path: '/cities',
	method: 'GET',
	collection: true,
	template: city.template
};

module.exports = [city, cities]